import { Team } from './team/team.model';

export interface AppState {
  readonly team: Team;
}
