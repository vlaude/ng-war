import { Team } from './team.model';
import * as TeamActions from './team.actions';

const initialState: Team = {
  id: 1,
  name: 'Test',
  heroes: []
};


export function reducer(state: Team = initialState, action: TeamActions.Actions) {
  return state;
}
