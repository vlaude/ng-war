import { Action } from '@ngrx/store';

export const GET_TEAM = '[TEAM] Get';
export const GET_TEAM_SUCCESS = '[TEAM] Get success';
export const GET_TEAM_ERROR = '[TEAM] Get error';

export class GetTeam implements Action {
  readonly type = GET_TEAM;

  constructor(public payload: { id: number, pending: true, error: null }) {}
}

export type Actions = GetTeam;
