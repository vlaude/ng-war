import { Hero } from '../hero/hero.model';

export interface Team {
  id: number;
  name: string;
  heroes: Hero[];
}
