import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule
  ],
  exports: [
    MatToolbarModule
  ],
  declarations: []
})
export class MaterialModule { }
