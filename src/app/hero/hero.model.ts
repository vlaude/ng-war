export interface Hero {
  id: number;
  name: string;
  level: number;
  race: string;
  class: string;
}
