import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.state';
import { Observable } from 'rxjs';
import { Team } from './team/team.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  team$: Observable<Team>;

  constructor(private store: Store<AppState>) {
    this.team$ = store.select('team');
  }

  ngOnInit(): void {}

}
